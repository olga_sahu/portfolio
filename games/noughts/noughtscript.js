var board = document.getElementsByClassName('boardNoughts')[0], //Игровое поле
    play = document.getElementsByClassName('gameNoughts')[0], //Строка хода
    elements, element,
    player1 = true, //какой игрок сейчас ходит
    gameTable = [[null, null, null], [null, null, null], [null, null, null]], //игровая матрица
    playNumbers = 9, //количество ходов, которые остались
    winner = null;

play.innerText = 'Сейчас ходит X';

//Игровое поле (генерация)
for (var i = 0; i < 9; i++) {
    elements = document.createElement('div');
    elements.classList.add('cellNoughts');
    element = document.createElement('div');
    element.classList.add('innerCellNoughts');
    element.onclick = tableNoughtsClick;
    element.setAttribute('x', (i % 3).toString());
    element.setAttribute('y', parseInt(i / 3).toString());
    elements.appendChild(element);
    board.appendChild(elements);
}
document.getElementsByClassName('btnNoughts')[0].onclick = reset;

//Событие нажатия на ячейку
function tableNoughtsClick() {
    //проверка содержимого ячейки
    if (this.innerText == '') {
        this.innerText = player1 ? 'X' : 'O';
        var y = this.getAttribute('y'), x = this.getAttribute('x');
        gameTable[y][x] = player1;
        playNumbers--;
        if ((gameTable[y][0] === player1 && gameTable[y][1] === player1 && gameTable[y][2] === player1) ||
            (gameTable[0][x] === player1 && gameTable[1][x] === player1 && gameTable[2][x] === player1) ||
            (gameTable[0][0] === player1 && gameTable[1][1] === player1 && gameTable[2][2] === player1) ||
            (gameTable[2][0] === player1 && gameTable[1][1] === player1 && gameTable[0][2] === player1)) {
            winner = player1;
        }
        player1 = !player1;
        play.innerText = player1 ? 'Сейчас ходит X' : 'Сейчас ходит O';
        if (playNumbers == 0 || winner !== null) {
            if (winner !== null) {
                    if (confirm('Победил ' + (winner ? 'X' : 'O') + '.\nЖелаете сыграть ещё?')) {
                        reset();
                    }
                } else if (confirm('Игра закончилась в ничью. \nЖелаете сыграть еще?')) {
                    reset();
                }
            }
        } 
    else {
            alert('Недопустимый ход');
        }
    }

    


//сброс параметров игры
function reset() {
        player1 = true; //кто сейчас ходит
        gameTable = [[null, null, null], [null, null, null], [null, null, null]]; //игровая матрица
        playNumbers = 9; //оставшиеся ходы
        winner = null;
        var tableNoughts = document.getElementsByClassName('innerCellNoughts');
        for (var i = 0; i < tableNoughts.length; i++) {
            tableNoughts[i].innerText = '';
        }
        play.innerText = 'Сейчас ходит X';
}
