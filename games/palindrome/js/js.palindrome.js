//'use strict';
var isPalindrome = function(s) {
    var time = performance.now();
    s=s.toLocaleLowerCase().match(/[\w\d]/g);
    var result = s.join('')===s.reverse().join('');
    time = performance.now() - time;
    return {result: result, t:time.toFixed(2)}
};


//var isPalindrome1Clean = function(s) { // 81 chars
//    s=s.toLocaleLowerCase().match(/[\w\d]/g);
//    return s.join('')===s.reverse().join('')
//};

//Подстановка готовых тестовых выражений в input
document.querySelector('#palBtn2').addEventListener('click', function () {
    var str = document.querySelectorAll('.preMadePalindrome');
    for (var i=0; i<str.length; i++) {
        if (str[i].checked === true) {
            var text = (str[i].parentNode).querySelector('span').innerText;
            var input = document.querySelector('#palindromeMainInput');
            input.focus();
            input.value = text;
            input.classList.remove('is-invalid');
            input.classList.add('is-valid');
        }
     }
});
//Обработчики для кнопок запуска теста
document.querySelector('#palBtn').addEventListener('click', function () {
    var block;
    if (this.parentNode.querySelector('.result')) {
        block = this.parentNode.querySelector('.result');
        block.innerHTML='';
        block.classList.remove('resultOk');
    } else {
        block = document.createElement('div');
    }
    var node = document.createElement('div');
    var test = isPalindrome(document.querySelector('#palindromeMainInput').value);
    node.innerText = 'Result = '+ test.result;
    block.classList.add('result');
    if (node.innerText == 'Result = true') {
        block.classList.add('resultOk');
    }
    block.appendChild(node);
    node = document.createElement('div');
    node.innerText = test.t + ' ms';
    block.appendChild(node);
    this.parentNode.appendChild(block);
});
