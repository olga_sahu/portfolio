 $(document).ready(function() {
            let $listItems = $('.solutions-item'); // получить коллекцию li
            let $textItems = $('.solutions-subtext'); // получить коллекцию текстов
            $listItems.on('click', function() { // по клику на один из элементов li
                let $clickLi = $('.solutions-item').index(this); // получить порядковый номер элемента li в коллекции (по которому был клик)
                // удалить у всех элементов активный класс и добавить активный класс элементу, на который был клик
                $listItems.removeClass('solutions-item-active').eq($clickLi).addClass('solutions-item-active'); // удалить у всех элементов активный класс

                // удалить у всех текстов активный класс и добавить активный класс тексту, чей порядковый номер в коллекции соответствует номеру элемента, на котором был клик
                $textItems.removeClass('solutions-subtext-active').eq($clickLi).addClass('solutions-subtext-active');
            });
        });