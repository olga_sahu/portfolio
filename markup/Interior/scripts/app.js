$(document).ready(function() {
    
    
    $('.slider').addClass("hidden").viewportChecker({
        classToAdd: 'visible animated fadeIn',
        offset: 500
    });
    
    $('.featured').addClass("hidden").viewportChecker({
        classToAdd: 'visible animated shake',
        offset: 500
    });

    $('.promo-product').addClass("hidden").viewportChecker({
        classToAdd: 'visible animated bounce',
        offset: 500
    });

    $('.screenshots').addClass("hidden").viewportChecker({
        classToAdd: 'visible animated fadeIn',
        offset: 500
    });

    $('.trending').addClass("hidden").viewportChecker({
        classToAdd: 'visible animated fadeIn',
        offset: 500
    });

    $('.slider1').addClass("hidden").viewportChecker({
        classToAdd: 'visible animated fadeIn',
        offset: 500
    });

    $('.exclusive').addClass("hidden").viewportChecker({
        classToAdd: 'visible animated slideInUp',
        offset: 300
    });
    
    $('.mobile-app').addClass("hidden").viewportChecker({
        classToAdd: 'visible animated bounce',
        offset: 300
    });
    
     $('footer').addClass("hidden").viewportChecker({
        classToAdd: 'visible animated slideInUp',
        offset: 300
    });
    
});